<?php

declare(strict_types=1);

namespace Example\Calculator\Api\Data;

interface CalculationResultInterface
{
    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): CalculationResultInterface;

    /**
     * @return string
     */
    public function getStatus(): string;

    /**
     * @param string $result
     * @return $this
     */
    public function setResult(string $result): CalculationResultInterface;

    /**
     * @return string
     */
    public function getResult(): string;
}
