<?php

declare(strict_types=1);

namespace Example\Calculator\Api;

use Example\Calculator\Api\Data\CalculationResultInterface;

interface CalculatorInterface
{
    public const API_STATUS_OK = 'OK';
    public const API_STATUS_ERROR = 'ERROR'; //We choose to throw a standard error rather than json response due to spec
    public const API_CALCULATION_VALID_OPERATORS = [
        'add',
        'subtract',
        'multiply',
        'divide',
        'power',
    ];

    /**
     * As union types (int/float) is not supported yet. left and right have not been type bound
     *
     * @param float|int $left
     * @param float|int $right
     * @param string $operator
     * @param int $precision
     *
     * @return \Example\Calculator\Api\Data\CalculationResultInterface
     */
    public function calculate($left, $right, string $operator, int $precision = 2): CalculationResultInterface;
}
