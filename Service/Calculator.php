<?php

declare(strict_types=1);

namespace Example\Calculator\Service;

use Example\Calculator\Api\CalculatorInterface;
use Example\Calculator\Api\Data\CalculationResultInterface;
use Example\Calculator\Api\Data\CalculationResultInterfaceFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Webapi\Exception as WebapiException;

class Calculator implements CalculatorInterface
{
    /**
     * @var CalculationResultInterfaceFactory
     */
    private $calculationResult;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * Calculator constructor.
     * @param CalculationResultInterfaceFactory $calculationResult
     * @param SerializerInterface $serializer
     */
    public function __construct(
        CalculationResultInterfaceFactory $calculationResult,
        SerializerInterface $serializer
    ) {
        $this->calculationResult = $calculationResult;
        $this->serializer = $serializer;
    }

    /**
     * As union types (int/float) is not supported yet. left and right have not been type bound
     *
     * @param float|int $left
     * @param float|int $right
     * @param string $operator
     * @param int $precision
     *
     * @return \Example\Calculator\Api\Data\CalculationResultInterface
     * @throws WebapiException
     */
    public function calculate($left, $right, string $operator, int $precision = 2): CalculationResultInterface
    {
        $operator = strtolower($operator);
        $errors = $this->validateParams($left, $right, $operator, $precision);
        if ($errors) {
            throw new WebapiException(
                __(
                    'Errors in request, please correct these issues and try again: %1',
                    implode(' | ', $errors)
                )
            );
        }

        //Convert numerics to strings - this is required for working with precision numbers
        $leftAsString = (string)$left;
        $rightAsString = (string)$right;

        $result = $this->calculationResult->create();
        $result->setStatus(static::API_STATUS_OK);
        //We are purposely not handling the throw that may occur in handleCalculation so it presents to our API user
        $result->setResult($this->handleCalculation($leftAsString, $rightAsString, $operator, $precision));

        return $result;
    }

    /**
     * @param $left
     * @param $right
     * @param string $operator
     * @param int $precision
     *
     * @return array
     */
    private function validateParams($left, $right, string $operator, int $precision): array
    {
        //Instead of throwing an exception on each check it is better for us and a user to see each error at once
        //rather than them having to make multiple calls to correct each issue
        $errors = [];

        //This check shouldn't fire due with docblock typing and the generated API functionality - included to be safe
        if (!is_numeric($left) || !is_numeric($right)) {
            $errors[] = __(
                'One of the provided values are not in a numeric format. Was provided left: %1, and right: %2',
                $left,
                $right
            );
        }

        if (!in_array($operator, static::API_CALCULATION_VALID_OPERATORS)) {
            $errors[] = __(
                '"%1" is not a valid operator in use by this calculator. Accepted operators are %2',
                $operator,
                implode(', ', static::API_CALCULATION_VALID_OPERATORS)
            );
        }

        if ($precision < 0) {
            $errors[] = __(
                'Precision must be a positive integer, "%1" given',
                $precision
            );
        }

        return $errors;
    }

    /**
     * When working with precision numbers, to be truly accurate you need to use arbitrary precision math functions
     * These require string inputs
     *
     * @param string $leftAsString
     * @param string $rightAsString
     * @param string $operator
     * @param int $precision
     *
     * @return string|null
     * @throws WebapiException
     */
    private function handleCalculation(
        string $leftAsString,
        string $rightAsString,
        string $operator,
        int $precision
    ): ?string {
        $value = null;
        switch ($operator) {
            case 'power':
                $value = bcpow($leftAsString, $rightAsString, $precision);
                break;
            case 'divide':
                $value = bcdiv($leftAsString, $rightAsString, $precision);
                break;
            case 'multiply':
                $value = bcmul($leftAsString, $rightAsString, $precision);
                break;
            case 'subtract':
                $value = bcsub($leftAsString, $rightAsString, $precision);
                break;
            case 'add':
                $value = bcadd($leftAsString, $rightAsString, $precision);
                break;
        }

        if ($value === null) {
            throw new WebapiException(
                __(
                    'Invalid operator provided "%1": Acceptable options are: %2',
                    $operator,
                    implode(', ', static::API_CALCULATION_VALID_OPERATORS)
                )
            );
        }

        return $value;

        //result should be numeric but floats strip trailing zeros leading to inconsistent returns
        //If true numeric return was a hard requirement we could update the return to below and all associated typing
        //return (float) number_format((float) $value, $precision, '.', ',');
    }
}
