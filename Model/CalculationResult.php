<?php

declare(strict_types=1);

namespace Example\Calculator\Model;

use Example\Calculator\Api\Data\CalculationResultInterface;

class CalculationResult implements CalculationResultInterface
{
    /**
     * @var string
     */
    private $status = '';

    /**
     * @var string
     */
    private $result = '';

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): CalculationResultInterface
    {
        $status = trim($status);
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $result
     * @return $this
     */
    public function setResult(string $result): CalculationResultInterface
    {
        $result = trim($result);
        $this->result = $result;

        return $this;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }
}
