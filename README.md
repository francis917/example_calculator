Example API Calculator Module
==================
_Author: Francis M. Gallagher_

This is an example module to show off how I would produce a simple API route.
The API supports adding, subtracting, multiplying, dividing two numbers; as well as calculating one number to the power of the other
The API runs on route `/V1/api/rce/calculator` meaning you can target it via `www.yoururlhere.com/rest/V1/api/rce/calculator`


###Installation process:
1. Clone repository to host machine 
2. Copy Example_Calculator module into your app/code folder
3. run `php bin/magento module:enable Example_Calculator && php bin/magento set:up && php bin/magento set:di:com && php bin/magento c:c`
    * Ensure to change `php` if you use a specific version alias such as `php-7.3`

Note: Composer instructions would be to run `composer require example/calculator` in the magento root folder however this module has not been made public to composer to avoid spoiling this test by having it available to others.

###Instructions
This module contains one route `/V1/api/rce/calculator` which only accepts POSTs
The accepted parameters are as follows

    * left: int/float (required)
    * right: int/float (required)
    * operator: string (required, acceptable options below)
        * 'add'
        * 'subtract'
        * 'multiply'
        * 'divide'
        * 'power'
    * precision: int (optional, default = 2)

An example POST would end up looking like:
```
https://local.testsite.com/rest/V1/api/rce/calculator?left=14&right=10&operator=subtract&precision=2
```

using JS within the Magento FE you may do 
```
storage.post(
    'rest/V1/api/rce/calculator/',
    JSON.stringify({
        left: 12,
        right: 13,
        operator: 'add',
        precision: 0
    })
)
.done(function (response) {
    var responseObject = JSON.parse(response);
})
.fail(function (response) {
    console.log({
        content: $t('API Called Failed.')
    });
});
```


###Running the Tests
Before running the tests please ensure your environment is set up to run REST API tests as described in the core documentation [here](https://devdocs.magento.com/guides/v2.4/get-started/web-api-functional-testing.html) 


To run all API tests run the following
```
vendor/bin/phpunit --config /path/to/magento/dev/tests/api-functional/phpunit_rest.xml
```

To save time however you can specifically run tests against this module by targeting the API test file
```
vendor/bin/phpunit --config /path/to/magento/dev/tests/api-functional/phpunit_rest.xml app/code/Example/Calculator/Test/Api/CalculatorInterfaceTest.php
```

### Other
I would like to note that the given spec stated that the result part of the return body is a numeric: 123.45 was given as an example.
However I have purposely chosen to return a string instead; because we are working with precision anyone using the API may be expecting a specific format to be returned based on the precision value. The problem with this is that floats will strip all tailing zeros, so a 15.000 will return as 15 which could cause issues for those expecting to work with the precision they set. 
