<?php

declare(strict_types=1);

namespace Example\Calculator\Test\Api;

use Example\Calculator\Api\CalculatorInterface;
use Magento\Framework\Webapi\Rest\Request;
use Magento\TestFramework\TestCase\WebapiAbstract;

class CalculatorInterfaceTest extends WebapiAbstract
{

    /**
     * @return array[]
     */
    private function getServiceInfo(): array
    {
        return [
            'rest' => [
                'resourcePath' => '/V1/api/rce/calculator',
                'httpMethod' => Request::HTTP_METHOD_POST
            ]
        ];
    }

    public function testCalculateAdd(): void
    {
        $response = $this->_webApiCall(
            $this->getServiceInfo(),
            [
                'left' => 10,
                'right' => 10,
                'operator' => 'add',
                'precision' => 0
            ]
        );

        self::assertNotNull($response);
        self::assertArrayHasKey('status', $response);
        self::assertArrayHasKey('result', $response);

        self::assertNotNull($response['status']);
        self::assertNotNull($response['result']);

        self::assertSame('OK', $response['status']);
        self::assertSame('20', $response['result']);
    }

    public function testCalculateSubtract(): void
    {
        $response = $this->_webApiCall(
            $this->getServiceInfo(),
            [
                'left' => 10,
                'right' => 10,
                'operator' => 'subtract',
                'precision' => 0
            ]
        );

        self::assertNotNull($response);
        self::assertArrayHasKey('status', $response);
        self::assertArrayHasKey('result', $response);

        self::assertNotNull($response['status']);
        self::assertNotNull($response['result']);

        self::assertSame('OK', $response['status']);
        self::assertSame('0', $response['result']);
    }

    public function testCalculateMultiply(): void
    {
        $response = $this->_webApiCall(
            $this->getServiceInfo(),
            [
                'left' => 10,
                'right' => 10,
                'operator' => 'multiply',
                'precision' => 0
            ]
        );

        self::assertNotNull($response);
        self::assertArrayHasKey('status', $response);
        self::assertArrayHasKey('result', $response);

        self::assertNotNull($response['status']);
        self::assertNotNull($response['result']);

        self::assertSame('OK', $response['status']);
        self::assertSame('100', $response['result']);
    }

    public function testCalculateDivide(): void
    {
        $response = $this->_webApiCall(
            $this->getServiceInfo(),
            [
                'left' => 10,
                'right' => 10,
                'operator' => 'divide',
                'precision' => 0
            ]
        );

        self::assertNotNull($response);
        self::assertArrayHasKey('status', $response);
        self::assertArrayHasKey('result', $response);

        self::assertNotNull($response['status']);
        self::assertNotNull($response['result']);

        self::assertSame('OK', $response['status']);
        self::assertSame('1', $response['result']);
    }

    public function testCalculatePower(): void
    {
        $response = $this->_webApiCall(
            $this->getServiceInfo(),
            [
                'left' => 10,
                'right' => 10,
                'operator' => 'power',
                'precision' => 0
            ]
        );

        self::assertNotNull($response);
        self::assertArrayHasKey('status', $response);
        self::assertArrayHasKey('result', $response);

        self::assertNotNull($response['status']);
        self::assertNotNull($response['result']);

        self::assertSame('OK', $response['status']);
        self::assertSame('10000000000', $response['result']);
    }

    public function testCalculatePrecision(): void
    {
        $response = $this->_webApiCall(
            $this->getServiceInfo(),
            [
                'left' => 10.4321,
                'right' => 10.2345,
                'operator' => 'add',
                'precision' => 4
            ]
        );

        self::assertNotNull($response);
        self::assertArrayHasKey('status', $response);
        self::assertArrayHasKey('result', $response);

        self::assertNotNull($response['status']);
        self::assertNotNull($response['result']);

        self::assertSame('OK', $response['status']);
        self::assertSame('20.6666', $response['result']);
    }

    public function testCalculateIncorrectTypeGiven(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage(
            'The \"s\" value\'s type is invalid. The \"float\" type was expected. Verify and try again.'
        );

        $this->_webApiCall(
            $this->getServiceInfo(),
            [
                'left' => 's',
                'right' => 10,
                'operator' => 'add',
                'precision' => 0
            ]
        );
    }

    public function testCalculateIncorrectOperator(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage(
            sprintf(
                'Errors in request, please correct these issues and try again:' .
                ' \"ads\" is not a valid operator in use by this calculator. Accepted operators are %s',
                implode(', ', CalculatorInterface::API_CALCULATION_VALID_OPERATORS)
            )
        );

        $this->_webApiCall(
            $this->getServiceInfo(),
            [
                'left' => 10,
                'right' => 10,
                'operator' => 'ads',
                'precision' => 0
            ]
        );
    }

    public function testCalculateNegativePrecisionValue(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage(
            'Errors in request, please correct these issues and try again:' .
            ' Precision must be a positive integer, \"-2\" given',
        );

        $this->_webApiCall(
            $this->getServiceInfo(),
            [
                'left' => 10,
                'right' => 10,
                'operator' => 'add',
                'precision' => -2
            ]
        );
    }
}
